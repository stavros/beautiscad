#!/usr/bin/env python3
import os
import subprocess
import sys
import tempfile

CONFIG = """
BasedOnStyle: Mozilla
ColumnLimit: 100
# SortIncludes: true
IndentWidth: 4
AccessModifierOffset: -4
ContinuationIndentWidth: 4
TabWidth: 4
UseTab: Never
""".strip()


def get_style_string() -> str:
    no_comments = [l for l in CONFIG.split("\n") if not l.strip().startswith("#")]
    split_lines = [l.split(":") for l in no_comments]
    return "{" + ", ".join([": ".join(x) for x in split_lines]) + "}"


def main(filename):
    if filename == "-":
        fd, fn = tempfile.mkstemp(text=True)
        with open(fn, "wt") as outfile:
            outfile.write(sys.stdin.read())
        filename = fn
    else:
        fn = None

    subprocess.run(["clang-format", "-style", get_style_string(), "-i", filename])

    if fn:
        with open(fn, "rt") as infile:
            sys.stdout.write(infile.read())
            sys.stdout.flush()
        os.unlink(fn)


if __name__ == "__main__":
    main(sys.argv[1])
