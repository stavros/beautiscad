# BeautiSCAD

An OpenSCAD code beautifier based on clang-format.


## Usage

To use this, just install `clang-format`, copy `beautiscad` somewhere and run it:

```
./beautiscad <your_openscad_file.scad>
```

You can also cat the file to `beautiscad`:

```
cat somefile.scad | ./beautiscad > outfile.scad
```

That's it!


## Future features

I am currently too lazy to make this into a proper package, but I might if there's demand.
The script will also possibly break imports by formatting them in a weird way, I don't know
enough about OpenSCAD to fix that, but I will if someone is bothered by it.


-- Stavros
